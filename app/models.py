from django.db import models

# Create your models here.
class Page(models.Model):
        recurso = models.CharField(max_length = 100)
        contenido = models.CharField(max_length = 10000)

        def __str__(self):
            return(self.recurso)

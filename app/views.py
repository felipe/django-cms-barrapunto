from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Page
import urllib.parse
from . import parsear_barrapunto

Html_not_found = '<html><body>\
<h1>Page {recurso} not found </h1></br></br>\
{formulario}\
</body></html>'

Html_page = '<html><body>\
<h1><u>{recurso}</u></h1></br><ul><p>\
{contenido}</p></ul></br>\
{formulario}\
</body></html>'

formulario = '<meta charset="UTF-8"><form action="" method="post">\
<p>Recurso: <input type="text" name="recurso" value="{recurso}" size="40"></p>\
<p>Contenido: <input type="text" name="contenido" size="40"></p>\
<p><input type="submit" value="Enviar"></p>'

# Create your views here.
def listar(request):
    lista = ''
    lista = "<h1>Lista de recursos</h1></br><ul>"
    for page in Page.objects.all():
        lista += "*<a href='" + (page.recurso) + "'>" + (page.recurso) + "</a></br>"
    return HttpResponse(lista + "</ul>"  + "\n" + parsear_barrapunto.main())

@csrf_exempt
def mostrar_recurso(request, name):
    if request.method == 'GET':
        print(request.path)
        recurso = request.path.strip('/')

        formulario_fill = formulario.format(recurso = recurso)
        try:
            Page.objects.get(recurso = recurso)
        except Page.DoesNotExist:
            html_fill = Html_not_found.format(recurso = recurso, formulario = formulario_fill)
            return HttpResponse(html_fill)

        contenido = Page.objects.get(recurso = recurso).contenido
        html_fill = Html_page.format(recurso = recurso, contenido = contenido, formulario = formulario_fill)
        return HttpResponse(html_fill + "\n" + parsear_barrapunto.main())


    if request.method == 'POST':
        recurso = request.POST['recurso']
        contenido = request.POST['contenido']
        try:
            s = Page.objects.get(recurso = recurso)
            s.delete()
            s = Page(recurso = recurso, contenido = contenido)
            s.save()
        except Page.DoesNotExist:
            s = Page(recurso = recurso, contenido = contenido)
            s.save()

        return HttpResponse("Hemos añadido el nuevo recurso: " + recurso)

#!/usr/bin/python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
import urllib.request

class CounterHandler(ContentHandler):

    def __init__ (self):
        self.pepe = "jola"
        self.page = ("<h1>Noticias</h1></br>\n")
        self.inContent = 0
        self.theContent = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inContent = 1

    def endElement (self, name):
        if name == 'item':
            self.inContent = 0
        elif name == 'title' and self.inContent:
            self.titulo = self.theContent.strip('\n')
            self.page += ("  <a href=")
            self.theContent=""
        elif name == 'link' and self.inContent:
            self.url = self.theContent.strip('\n')
            self.page += (self.url + " target=_blank>" + self.titulo + "</a></br>\n")
            self.theContent=""
        if self.inContent:
            self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog
def main():
    BarraPuntoParser = make_parser()
    BarraPuntoHandler = CounterHandler()
    BarraPuntoParser.setContentHandler(BarraPuntoHandler)

    url = 'http://barrapunto.com/barrapunto.rss'
    xmlFile = urllib.request.urlopen(url)
    BarraPuntoParser.parse(xmlFile)

    return BarraPuntoHandler.page

if __name__ == '__main__':
    main()
